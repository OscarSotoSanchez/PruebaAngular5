import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'error',
  templateUrl: './error.html'
})

export class ErrorComponent {
  constructor(private location: Location ) { }
}
