import { Routes, RouterModule } from '@angular/router';

import { UserIndexComponent } from './users/user-index.component';
import { UserViewComponent } from './users/user-view.component';
import { UserEditComponent } from './users/user-edit.component';
import { UserNewComponent } from './users/user-new.component';

import { ErrorComponent } from './error/error.component';

const appRoutes = [
  { path: '', redirectTo: 'usuarios', pathMatch: 'full' },
  { path: 'usuarios', component: UserIndexComponent },
  { path: 'usuarios/:delete', component: UserIndexComponent },
  { path: 'usuario/nuevo', component: UserNewComponent },
  { path: 'usuario/:id', component: UserViewComponent },
  { path: 'usuario/editar/:id', component: UserEditComponent },
  { path: '**', component: ErrorComponent }
]

export const routing = RouterModule.forRoot(appRoutes);
