import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { User } from '../interfaces/user.component';

const BASE_URL = 'http://localhost:3000/users/';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    public getUsers() {
        return this.http.get(BASE_URL);
    }

    public getUserPagination(page: number): Observable<any>{
        return this.http.get(BASE_URL + "?_page=" + page, { observe: 'response' });
    }

    public getUser(id: number): Observable<any> {
        return this.http.get(BASE_URL + id);
    }

    public addUser(user: User){
        return this.http.post(BASE_URL, user);
    }

    public updateUser(user: User){
        return this.http.put(BASE_URL + user.id, user);
    }

    public deleteUser(id: number) {
        return this.http.delete(BASE_URL + id);
    }
}