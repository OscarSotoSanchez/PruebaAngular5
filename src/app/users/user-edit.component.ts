import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

import { UserService } from '../services/user.service';

import { User } from '../interfaces/user.component';

@Component({
  selector: 'useredit',
  templateUrl: './user-view.html',
  styleUrls: ['./user-view.css']
})

export class UserEditComponent {
  private userId: number;
  private user: Observable<User>;
  private userValidation: FormGroup;
  private wrongUser: boolean = false;

  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute, private location: Location, private fb: FormBuilder) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params["id"];
        this.validatorForm(null, null, null);
        this.loadUser();
      }
    );
  }

  private validatorForm(name: string, surname: string, age: number) {
    this.userValidation = this.fb.group({
      'name': [name, Validators.required],
      'surname': [surname, Validators.required],
      'age': [age, [Validators.required, Validators.pattern('^([0-9])*$')]]
    });
  }

  private formSubmit(name: string, surname: string, age: number) {
    let user = {
      "id": this.userId,
      "name": name,
      "surname": surname,
      "age": age
    }

    this.userService.updateUser(user).subscribe(
      (user: User) => {
        this.router.navigate(['/usuario', user.id]);
      }
    );
  }

  private loadUser() {
    this.user = this.userService.getUser(this.userId);

    this.user.subscribe(
      (user: User) => { this.validatorForm(user.name, user.surname, user.age) },
      error => { this.wrongUser = true }
    );
  }

  private isDisabled(): boolean {
    return false;
  }
}