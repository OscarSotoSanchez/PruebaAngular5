import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../services/user.service';

import { User } from '../interfaces/user.component';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'userview',
  templateUrl: './user-view.html',
  styleUrls: ['./user-view.css']
})

export class UserViewComponent {
  private isView: boolean = true;
  private userId: number;
  private user: Observable<User>;
  private userValidation: FormGroup;
  private wrongUser: boolean = false;

  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute, private location: Location, private fb: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params["id"];
        this.loadUser();
        this.validatorForm();
      }
    );
  }

  private validatorForm() {
    this.userValidation = this.fb.group({
      'name': [{ value: '', disabled: true }],
      'surname': [{ value: '', disabled: true }],
      'age': [{ value: '', disabled: true }]
    });
  }

  private loadUser() {
    this.user = this.userService.getUser(this.userId);

    this.user.isEmpty().subscribe(
      result => { },
      error => { this.wrongUser = true }
    );
  }

  private deleteUser(id: number) {
    this.userService.deleteUser(id).subscribe(
      result => {
        this.router.navigate(['usuarios/delete']);
      }
    );
  }

  private openDeleteUserDialog(content) {
    this.modalService.open(content);
  }

  private deleteUserDialogYes() {
    this.deleteUser(this.userId);
  }

  private isDisabled(): boolean {
    return true;
  }
}