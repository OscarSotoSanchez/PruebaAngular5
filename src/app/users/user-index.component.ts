import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';

import { UserService } from '../services/user.service';

import { User } from '../interfaces/user.component';

@Component({
  selector: 'userindex',
  templateUrl: './user-index.html',
  styleUrls: ['./user-index.css']
})

export class UserIndexComponent {
  private users: User[] = [];
  private removeUserId: number;
  private removeUser: boolean = false;
  private page = 1;
  private elements = 0;

  constructor(private userService: UserService, private router: Router, private activatedRoute: ActivatedRoute, private modalService: NgbModal, private location: Location) { }

  ngOnInit() {
    this.loadUsers(0);

    this.activatedRoute.params.subscribe(
      params => {
        let isDelete = params["delete"];

        if (isDelete !== undefined) {
          this.showAlert();
          this.location.replaceState("/usuarios");
        }
      }
    );
  }

  private loadUsers(page: number) {
    this.userService.getUserPagination(page).subscribe(
      response => {
        this.elements = response.headers.get('X-Total-Count');
        this.users = response.body;
      }
    )
  }

  private deleteUser(id: number) {
    this.userService.deleteUser(id).subscribe(
      result => {
        this.loadUsers(this.page);
        this.showAlert();
      }
    )
  }

  private loadPage(event: number) {
    this.loadUsers(event);
  }

  private openDeleteUserDialog(content, id: number) {
    this.modalService.open(content);
    this.removeUserId = id;
  }

  private deleteUserDialogYes() {
    this.deleteUser(this.removeUserId);
  }

  private showAlert() {
    if (this.removeUser !== true) {
      this.removeUser = true;
      setTimeout(() => this.closeAlert(), 10000);
    }
  }
  private closeAlert() {
    this.removeUser = false;
  }
}