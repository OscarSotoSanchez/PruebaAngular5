import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

import { UserService } from '../services/user.service';

import { User } from '../interfaces/user.component';

@Component({
  selector: 'usernew',
  templateUrl: './user-view.html',
  styleUrls: ['./user-view.css']
})

export class UserNewComponent {
  private isNew = true;
  private userValidation: FormGroup;

  constructor(private userService: UserService, private location: Location, private router: Router, private activatedRoute: ActivatedRoute, private fb: FormBuilder) { }

  ngOnInit() {
    this.validatorForm();
  }

  private validatorForm() {
    this.userValidation = this.fb.group({
      'name': [null, Validators.required],
      'surname': [null, Validators.required],
      'age': [null, [Validators.required, Validators.pattern('^([0-9])*$')]]
    });
  }

  private formSubmit(name: string, surname: string, age: number) {
    let user = {
      "name": name,
      "surname": surname,
      "age": age
    }

    this.userService.addUser(user).subscribe(
      (user: User) => {
        this.router.navigate(['/usuario', user.id]);
      }
    );
  }

  private isDisabled(): boolean {
    return false;
  }
}