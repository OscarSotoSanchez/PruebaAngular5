# PruebaAngular5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Prueba con Angular 5

* Simular un backend (RESTful) que acceda a una lista simple de usuarios (nombres, apellidos y edad), para el manejo de los datos se puede usar un archivo JSON o json-server. 
* Desarrollar los servicios (RESTful) en NG5 para conectarse al "backend".
* Desarrollar una interfaz que permita listar, crear, editar y eliminar usuarios, la página de inicio deberá ser el listado de usuarios y con botones para acceder a las funcionalidades mencionadas anteriormente.

## Fichero JSON Base de Datos 
[Fichero con datos de ejemplo](src/db/db.json)

`json-server --watch db.json`

## Capturas de pantalla funcionamiento aplicación

### Index, listado de usuarios

#### Sin usuarios en la base de datos

![indexNoUser](screenshot/index-no-usuario.png)

#### Con usuarios en la base de datos

![index](screenshot/index.png)

### Visualización usuario
![viewuser](screenshot/verusuario.png)

### Añadir usuario
![adduser](screenshot/nuevousuario.png)

### Editar usuario
![edituser](screenshot/editarusuario.png)

### Eliminar usuario
![deleteuseralert](screenshot/alertaborrar.png)
![deleteuser](screenshot/borrado.png)

### Control de errores

#### Usuario incorrecto

![wronguser](screenshot/usuarioincorrecto.png)

#### Error 404

![404](screenshot/404.png)